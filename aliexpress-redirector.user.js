// ==UserScript==
// @name        Aliexpress redirector
// @description Redirects from national versions of aliexpress to global english version. No more switching!
// @namespace globalbus
// @downloadURL https://bitbucket.org/globalbus/aliexpress-redirector/downloads/aliexpress-redirector.user.js
// @match     http://*.aliexpress.com/*
// @match     https://*.aliexpress.com/*
// @run-at document-start
// @version     11
// @grant       GM_xmlhttpRequest
// @grant       GM_getValue
// @grant       GM_setValue
// @connect  m.aliexpress.com
// ==/UserScript==

//made by globalbus - Jan Filipski
var targethost = "www.aliexpress.com";

eraseCookie ("xman_us_f"); //always erase this cookie. Fucks up search results!

//language switching
if(getCookie("intl_locale")!=="en_US" && !window.location.host.startsWith('feedback')){
    eraseCookie ("intl_locale");
    eraseCookie ("aep_usuc_f");
    window.location.host=targethost;
    window.search='';
}

var qs = parseQueryParameters(window.location.search.substr(1).split('&'));
var prohibitedParams = ['aff_platform', 'sk', 'cpt', 'aff_trace_key', 'af', 'cn', 'cv', 'afref', 'ws_test'];
var contains = false;
prohibitedParams.forEach(function(element){
    if(qs[element]!==undefined){
        contains = true;
        delete qs[element];
    }
});


if(contains){
    eraseCookie ("aeu_cid");
    var newQuery='?';
    Object.keys(qs).forEach(function (element){
        newQuery = newQuery + element + '=' + qs[element]+'&';
    });
    newQuery = newQuery.substring(0, newQuery.length - 1);
    window.location.search=newQuery;
}
if(/\/item\/.*/.test(window.location.pathname)){
    document.addEventListener("DOMContentLoaded", function(event) {
        var element = document.getElementsByClassName("mobile-discount-trigger")[0];
        if(element!==null){
            getMobilePrice(window.location.href, function(price){
                element.textContent += ' - ' + price;
            });
        }
    });
}
var outer;
document.addEventListener("DOMContentLoaded", function(event) {
    var filterBar = document.getElementById("filter-options");
    if(filterBar !==null){
        var text = document.createTextNode("Show final prices");
        var checkbox = document.createElement("i");
        checkbox.className = "check-icon";
        var inner = document.createElement("span");
        inner.appendChild(text);
        outer = document.createElement("a");
        switchButton(GM_getValue('final-price'));
        outer.onclick = showFinalPricesButtonCallback;
        outer.appendChild(checkbox);
        outer.appendChild(inner);
        filterBar.appendChild(outer);
    }
});
function switchButton(value){
    if(value===false){
        outer.className = "filter-item";
    }
    else{
        outer.className = "filter-item selected";
        showFinalPrices();
    }
}

function showFinalPricesButtonCallback(){
    var savedValue = GM_getValue('final-price');
    if(savedValue===undefined){
        savedValue=false;
    }
    else {
        savedValue=!savedValue;
    }
    GM_setValue('final-price', savedValue);
    switchButton(savedValue);
    if(!savedValue){
        window.location.reload();
    }
}
// code, symbol, value, postfix
//US $139.99 - 160.99
//var priceRegex =/(.*)?\ (.)(\d+\.\d+)(.*)?/;
var priceRegex=/(\w+)\ (.)(\d*,?\d+\.\d+)(\ \-\ (\d+\.\d+))?/;
function showFinalPrices(){
    var externalList = document.getElementById("hs-list-items");
    var innerList = document.getElementById("hs-below-list-items");
    if(externalList.tagName=="UL"){
        var elementsToCopy = innerList.children[1];
        var scriptTemp = innerList.children[0];
        innerList.innerHTML='';
        innerList.appendChild(scriptTemp);
        Array.prototype.forEach.call(elementsToCopy.children, function (element){
            externalList.appendChild(element);
        });
        innerList=externalList;
    }
    var listItems=innerList;
    var pricesMap = new Array();
    Array.prototype.forEach.call (listItems.children, function (element){

        var shippingElement = element.getElementsByClassName('pnl-shipping')[0];
        var priceElement = element.getElementsByClassName('price-m')[0];
        var priceDecimal = getPriceValue('value',priceElement);
        var finalPrice= new Array();
        if(shippingElement!==undefined)
        {
            var shippingDecimal = getPriceValue('value',shippingElement);
            for(var i=0;i<priceDecimal.length;i++){
                finalPrice[i] = (shippingDecimal[0] + priceDecimal[i]).toFixed(2);
            }
            replaceValue('value', priceElement, finalPrice);
        }
        else{
            finalPrice = priceDecimal;
        }
        pricesMap.push([finalPrice[0],element]);

    });
    if(/price.*/.test(qs.SortType)){
        listItems.innerHTML='';
        if(qs.SortType==='price_asc'){
            pricesMap.sort(function(a,b){return a[0]-b[0];});
        }
        else{
            pricesMap.sort(function(a,b){return b[0]-a[0];});
        }
        for(var i=0;i<pricesMap.length;i++){
            listItems.appendChild(pricesMap[i][1]);
        }
    }
}
function getPriceValue(clas, element){
    var priceElement = element.getElementsByClassName(clas)[0];
    if(priceElement!==undefined){
        var match = priceRegex.exec(priceElement.textContent);
        return [parseFloat(match[3]), parseFloat(match[5])];
    }
}
function replaceValue(clas, element, value){
    element = element.getElementsByClassName(clas)[0];
    if(element!==undefined){
        if(value[1]=="NaN"){
            element.textContent=element.textContent.replace(priceRegex, "Final $1 $2"+value[0]);
        }
        else{
            element.textContent=element.textContent.replace(priceRegex, "Final $1 $2"+value[0]+" - "+value[1]);
        }

    }
}
function getMobilePrice(url, callback){
    var loc = document.createElement('a');
    loc.href=url;
    loc.search="";
    loc.host = 'm.aliexpress.com';
    GM_xmlhttpRequest({
        method: "GET",
        url: loc.href,
        onload: function(response) {
            if (response.status == 200) {
                var el = document.createElement( 'html' );
                el.innerHTML =response.responseText;
                var price = el.getElementsByClassName('ms-promotion');
                if(price[0]!==undefined)
                {
                    price = price[0].getElementsByTagName('div')[0].textContent;
                    var match = priceRegex.exec(price);
                    callback(match[2]+match[3]);
                }
            }
        }
    });

}
function getLocation(href) {
    var l = document.createElement("a");
    l.href = href;
    return l;
}

function parseQueryParameters(a) {
    if (a === "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
        var p=a[i].split('=', 2);
        if (p.length == 1){
            b[p[0]] = "";
        }
        else{
            b[p[0]] = p[1].replace(/\+/g, " ");
        }
    }
    return b;
}
function clearAllCookies(){
    var cookies = parseQueryParameters(document.cookie.split('; '));
    Object.keys(cookies).forEach(function (element){
        eraseCookie(element);
    });
    window.location.reload();
}

//copied from http://stackoverflow.com/questions/2194473/can-greasemonkey-delete-cookies-from-a-given-domain
function eraseCookie (cookieName) {
    //--- ONE-TIME INITS:
    //--- Set possible domains. Omits some rare edge cases.?.
    var domain      = document.domain;
    var domain2     = document.domain.replace (/^www\./, "");
    var domain3     = document.domain.replace (/^(\w+\.)+?(\w+\.\w+)$/, "$2");

    //--- Get possible paths for the current page:
    var pathNodes   = location.pathname.split ("/").map ( function (pathWord) {
        return '/' + pathWord;
    } );
    var cookPaths   = [""].concat (pathNodes.map ( function (pathNode) {
        if (this.pathStr) {
            this.pathStr += pathNode;
        }
        else {
            this.pathStr = "; path=";
            return (this.pathStr + pathNode);
        }
        return (this.pathStr);
    } ) );

    ( eraseCookie = function (cookieName) {
        //--- For each path, attempt to delete the cookie.
        cookPaths.forEach ( function (pathStr) {
            //--- To delete a cookie, set its expiration date to a past value.
            var diagStr     = cookieName + "=" + pathStr + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;";
            document.cookie = diagStr;

            document.cookie = cookieName + "=" + pathStr + "; domain=" + domain  + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;";
            document.cookie = cookieName + "=" + pathStr + "; domain=" + domain2 + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;";
            document.cookie = cookieName + "=" + pathStr + "; domain=" + domain3 + "; expires=Thu, 01-Jan-1970 00:00:01 GMT;";
        } );
    } ) (cookieName);
}
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}